# Game Puzzle
This repository is created to solve a game puzzle challenge given to me in an interview.

## Puzzle
The board has a fixed size of 5 squares (assume it is always square).The piece can move around the board in one of four directions ((N)orth, (E)ast, (S)outh and (W)est).

The piece will start in the bottom left corner of the board facing North.

The bottom left board square is indicated by the position 0 0, top left corner of the board is 0 4, bottom right is 4 0 and top right is therefore 4 4.
    
If you try to make a move that would result in the piece moving off the board the move will have no effect. For example moving North when you are already at the top of the board.

A player can issue a set of commands to the game which will result in an output of the location and direction of the piece after the moves. You choose to use the output format X Y Direction, so a piece that has moved two squares right and one north and is facing north would be in position 2 1 N. Commands are either a request to move forward (M) or to turn left (L) or right (R) by 90 degrees.
 

## Solution
> Please have minimal node version 10.0.0 installed.

To execute the solution, issue the command
```
> # install the application
>
> npm i
> 
> # run the application
>
> npm run start
>
# When pormpted to enter an input game command enter a valid one like `MRMLMRM`, `RMMMLMM`, `MMMMM`
```

(optional) To verify if code lints (airbnb style)
```
> npm run lint
```

To unit test the code, issue the command
```
> npm run test
```
