const readLine = require('readline');
const { Game } = require('./lib/game');

const interactives = readLine.createInterface({
  input: process.stdin,
  output: process.stdout
});

interactives.question('What is your game command? ', (command) => {
  try {
    const game = new Game();
    game.play(command);

    const { x, y, direction } = game.getPlayerPosition();
    console.log(`\nFinal position of the player is : ${x} ${y} ${direction}`);

  } catch (error) {
    console.log(error.message);
  }

  interactives.close();
});
