class Board {

  constructor() {
    this.size = {
      rows: 5,
      column: 5
    };
    this.allowedMoves = ['M', 'L', 'R'];
  }
}

module.exports = { Board };
