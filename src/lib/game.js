const { Board } = require('./board');
const { Player } = require('./player');

class Game {
  constructor() {
    this.board = new Board();
    this.player = new Player();
  }

  play(command) {
    this.command = this._preProcessCommand(command);
    [...this.command].forEach((move) => {
      this.player.play(this.board, move);
    });
  }

  getPlayerPosition() {
    return this.player.position;
  }

  _preProcessCommand(commands) {
    const processedCommand = commands.replace(/\s+/g, '').toUpperCase();
    [...processedCommand].forEach((command) => {
      if (!this.board.allowedMoves.includes(command)) {
        throw new Error(`*** Error: Allowed moves in the command are : ${this.board.allowedMoves}. Please try again with valid moves.`);
      }
    });

    return processedCommand;
  }
}

module.exports = { Game };
