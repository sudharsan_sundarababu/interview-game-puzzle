const directions = ['N', 'E', 'S', 'W'];

const newDirection = (currentDirection, directionOfMove) => {
  let newDirectionValue = directions.indexOf(`${currentDirection}`);

  if (directionOfMove === 'L') {

    newDirectionValue -= 1;
    if (newDirectionValue === -1) newDirectionValue = 3;
  } else if (directionOfMove === 'R') {

    newDirectionValue += 1;
    if (newDirectionValue > 3) newDirectionValue = 0;
  } else {
    throw new Error('*** ERR Invalid direction of move. Allowed values are L/R');
  }

  return (directions[newDirectionValue]);
};

const newCoordinates = (board, currentPosition) => {
  let { x, y } = currentPosition;
  const { direction } = currentPosition;

  switch (direction) {
    case 'N':
      if (y < board.size.rows - 1) y += 1;
      break;
    case 'E':
      if (x < board.size.column - 1) x += 1;
      break;
    case 'S':
      if (y > 0) y -= 1;
      break;
    case 'W':
      if (x > 0 && x < board.size.column) x -= 1;
      break;
    default:
      throw new Error('*** ERR invalid direction. Allowed values are N/E/S/W');
  }

  return { x, y };
};

const newPosition = (board, currentPosition, move) => {
  if (!move || !board || !currentPosition) {
    throw new Error('*** ERR invalid argument.');
  }

  const position = { ...currentPosition };


  if (['L', 'R'].includes(move)) {
    position.direction = newDirection(currentPosition.direction, move);
  }

  if (['M'].includes(move)) {
    const { x, y } = newCoordinates(board, position);
    position.x = x;
    position.y = y;
  }

  return (position);
};

module.exports = {
  newPosition,
  newDirection,
  newCoordinates
};
