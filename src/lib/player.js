const { newPosition } = require('./helpers');

class Player {

  constructor() {
    this.position = {
      x: 0,
      y: 0,
      direction: 'N'
    };
  }

  play(board, move) {
    this.position = newPosition(board, this.position, move);
  }
}

module.exports = { Player };
