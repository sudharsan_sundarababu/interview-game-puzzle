/* eslint-disable no-unused-vars */
const {
  describe, it, beforeEach
} = require('mocha');
const { assert } = require('chai');

const { Game } = require('../../src/lib/game');

describe('game.js', () => {

  describe('getPlayerPosition()', () => {
    let gameBoard;

    beforeEach(() => {
      gameBoard = new Game();
    });

    it('should return current player position', () => {
      let actualPlayerPosition;
      let expectedPlayerPosition;

      const inputOutputValues = [
        {
          input: 'MRMLMRM',
          output: { x: 2, y: 2, direction: 'E' }
        },
        {
          input: 'RMMMLMM',
          output: { x: 3, y: 2, direction: 'N' }
        },
        {
          input: 'MMMMM',
          output: { x: 0, y: 4, direction: 'N' }
        }
      ];
      inputOutputValues.forEach((set) => {
        gameBoard = new Game();
        gameBoard.play(set.input);
        actualPlayerPosition = gameBoard.getPlayerPosition();
        expectedPlayerPosition = set.output;
        assert.deepEqual(actualPlayerPosition, expectedPlayerPosition);
      });
    });

  });

  describe('_preProcessCommand()', () => {
    let gameBoard;

    beforeEach(() => {
      gameBoard = new Game();
    });

    it('should remove whitespaces in the command', () => {
      const command = 'r l    MMM r l rlr r';
      const actualPreProcessedCommand = gameBoard._preProcessCommand(command);
      const expectedPreProcessedCommand = 'RLMMMRLRLRR';
      assert.equal(actualPreProcessedCommand, expectedPreProcessedCommand);

    });

    it('should return command in upper case', () => {
      const command = 'r l    mmm r l rlr r';
      const actualPreProcessedCommand = gameBoard._preProcessCommand(command);
      const expectedPreProcessedCommand = 'RLMMMRLRLRR';
      assert.equal(actualPreProcessedCommand, expectedPreProcessedCommand);
    });

    it('should throw an error if any of the move is invalid', () => {
      const command = 'MRMLMRMXYZ';
      try {
        const actualPreProcessedCommand = gameBoard._preProcessCommand(command);
        assert.fail('Expected the function to throw an error. But no errors were thrown.');
      } catch (error) {
        assert.equal(error.message, '*** Error: Allowed moves in the command are : M,L,R. Please try again with valid moves.');
      }
    });

    it('should be able to process game commands with atleast 32 moves', () => {
      const command = 'rlmmmrlrrlmmmrlrrlmmmrlrrlmmmrlrrlmmmrlrrlmmmrlrrlmmmrlrrlmmmrlr';
      const actualPreProcessedCommand = gameBoard._preProcessCommand(command);
      const expectedPreProcessedCommand = 'RLMMMRLRRLMMMRLRRLMMMRLRRLMMMRLRRLMMMRLRRLMMMRLRRLMMMRLRRLMMMRLR';
      assert.equal(actualPreProcessedCommand, expectedPreProcessedCommand);
    });
  });

});
