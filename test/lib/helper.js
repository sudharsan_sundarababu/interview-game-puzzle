const {
  describe, it, before, beforeEach, afterEach
} = require('mocha');
const { assert } = require('chai');

const {
  newDirection,
  newCoordinates,
  newPosition
} = require('../../src/lib/helpers');

const { Board } = require('../../src/lib/board');

describe('helper.js', () => {
  describe('newDirection()', () => {

    it('should return new direction when the direction of move is changed in clockwise', () => {
      const returnDirection = newDirection('N', 'R');
      assert.equal(returnDirection, 'E');
    });

    it('should return new direction when the direction of move is changed in anti-clockwise', () => {
      const returnDirection = newDirection('N', 'L');
      assert.equal(returnDirection, 'W');
    });

    it('should throw an error if direction of move is invalid', () => {
      try {
        newDirection('N', 'X');
        assert.fail('Expected the function to throw an error. But no errors were thrown.');
      } catch (error) {
        assert.equal(error.message, '*** ERR Invalid direction of move. Allowed values are L/R');
      }
    });

  });

  describe('newCoordinates()', () => {

    let gameBoard;

    before(() => {
      gameBoard = new Board();
    });

    it('should return new co-ordinate when the current position is not in the boundary', () => {
      const currentCoordinates = { x: 2, y: 3, direction: 'N' };
      const newReturnedCoordinates = newCoordinates(gameBoard, currentCoordinates);
      const expectedPosition = { x: 2, y: 4 };
      assert.deepEqual(newReturnedCoordinates, expectedPosition, `Expected new position is ${expectedPosition} but received ${newCoordinates}`);
    });

    it('should return the same co-ordinate when the current position is in the boundary', () => {
      const currentCoordinates = { x: 4, y: 4, direction: 'N' };
      const newReturnedCoordinates = newCoordinates(gameBoard, currentCoordinates);
      const expectedPosition = { x: 4, y: 4 };
      assert.deepEqual(newReturnedCoordinates, expectedPosition, `Expected new co-ordinate is ${expectedPosition} but received ${newCoordinates}`);
    });

    it('should throw an error if current direction is invalid', () => {
      const currentCoordinates = { x: 4, y: 4, direction: 'X' };
      try {
        newCoordinates(gameBoard, currentCoordinates);
        assert.fail('Expected the function to throw an error. But no errors were thrown.');
      } catch (error) {
        assert.equal(error.message, '*** ERR invalid direction. Allowed values are N/E/S/W');
      }
    });
  });

  describe('newPosition()', () => {
    let gameBoard;

    beforeEach(() => {
      gameBoard = new Board();
    });

    afterEach(() => {
      gameBoard = new Board();
    });


    it('should return a new player co-ordinates in the board when the next move is change of direction ', () => {
      const currentPosition = { x: 2, y: 3, direction: 'N' };
      const move = 'R';
      const actualNewPostion = newPosition(gameBoard, currentPosition, move);
      const expectedNewPosition = { x: 2, y: 3, direction: 'E' };
      assert.deepEqual(actualNewPostion, expectedNewPosition, `Expected new position is ${expectedNewPosition} but received ${actualNewPostion}`);
    });

    it('should return new player co-ordinates in the board when the next move is a moving in the same direction ', () => {
      const currentPosition = { x: 2, y: 3, direction: 'N' };
      const move = 'M';
      const actualNewPostion = newPosition(gameBoard, currentPosition, move);
      const expectedNewPosition = { x: 2, y: 4, direction: 'N' };
      assert.deepEqual(actualNewPostion, expectedNewPosition, `Expected new position is ${expectedNewPosition} but received ${actualNewPostion}`);
    });

    it('should return the same player co-ordinates in the board when the player is in the board edge and the next move is a cell move ', () => {
      const currentPosition = { x: 4, y: 4, direction: 'N' };
      const move = 'M';
      const actualNewPostion = newPosition(gameBoard, currentPosition, move);
      const expectedNewPosition = { x: 4, y: 4, direction: 'N' };
      assert.deepEqual(actualNewPostion, expectedNewPosition, `Expected new position is ${expectedNewPosition} but received ${actualNewPostion}`);
    });

    it('should throw an error if board is invalid', () => {
      gameBoard = undefined;
      const currentPosition = { x: 4, y: 4, direction: 'N' };
      const move = 'M';

      try {
        newPosition(gameBoard, currentPosition, move);
        assert.fail('Expected the function to throw an error. But no errors were thrown.');

      } catch (error) {
        assert.equal(error.message, '*** ERR invalid argument.');
      }
    });

    it('should throw an currentPosition if board is invalid', () => {
      const currentPosition = null;
      const move = 'M';

      try {
        newPosition(gameBoard, currentPosition, move);
        assert.fail('Expected the function to throw an error. But no errors were thrown.');

      } catch (error) {
        assert.equal(error.message, '*** ERR invalid argument.');
      }
    });

    it('should throw an next move if board is undefined', () => {
      const currentPosition = { x: 4, y: 4, direction: 'N' };
      const move = undefined;

      try {
        newPosition(gameBoard, currentPosition, move);
        assert.fail('Expected the function to throw an error. But no errors were thrown.');

      } catch (error) {
        assert.equal(error.message, '*** ERR invalid argument.');
      }
    });

  });

});
